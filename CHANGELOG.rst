.. _releasenotes:

Release Notes
=============


0.0 series
----------


0.0.8 (2022-05-23)
~~~~~~~~~~~~~~~~~~

* Add basic SBML Support
* Refactoring the summary statistics structure to a more modular one
* Fix simulation destination in case of no summary statistics
* Fixed and improved documentation
* Fix and reformat Cobertura reports to be compatible with Gitlab 15.0



0.0.7 (2021-11-28)
~~~~~~~~~~~~~~~~~~

* Fix CI triggers
* Consolidate badges


0.0.6 (2021-11-28)
~~~~~~~~~~~~~~~~~~

* Add discrete distance to prior types
* Add basic unit tests
* Check code coverage via codecov.io and cobertura
* Apply the uncompromising Python code formatter black as pre-commit
* Apply automatic import sorting via isort as pre-commit


0.0.5 (2021-11-10)
~~~~~~~~~~~~~~~~~~

* Modernize package and CI:

  * Add setup.cfg and pyproject.toml for package build and dependencies
  * Add tox and pre-commit hooks for testing and code quality checks
    via tox.ini and .flake8
  * Enforce pep8 code quality subset via flake8
  * Add API docs to documentation
  * Add CHANGELOG.rst and CONTRIBUTE.rst
  * Rename master branch to main
  * Streamline documentation build via .readthedocs.yaml
  * Add scripts for build of external dependencies
  * Add automatic deployment to PyPI

* Allow custom executables in model creation


0.0.4 (2021-11-03)
~~~~~~~~~~~~~~~~~~

* Initial release on gitlab
* Add basic petab support


0.0.3 (2019-11-25)
~~~~~~~~~~~~~~~~~~

* Fix error with utf-8 encoding
* Update notebooks to recent changes


0.0.2 (2019-10-10)
~~~~~~~~~~~~~~~~~~

* Basic repository set up
* Added documentation on fitmulticell.readthedocs.io
* Implemented MorpheusModel class
* Designed a logo
* Added a basic usage example in doc/example


0.0.1 (2019-09-24)
~~~~~~~~~~~~~~~~~~

Initial preparatory release, no functionality
