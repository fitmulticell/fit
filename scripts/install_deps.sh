#!/bin/sh

# Update package lists
apt-get update

for par in "$@"; do
  case $par in

    doc)
      # install pandoc
      apt-get install pandoc -y
    ;;

    *)
      echo "Unknown argument: $par" >&2
      exit 1
    ;;

  esac
done
