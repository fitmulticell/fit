#!/bin/sh
# Download and build morpheus from source

git clone https://gitlab.com/morpheus.lab/morpheus.git morpheus
cd morpheus
git checkout develop
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX="morpheus" ..
make && make install
