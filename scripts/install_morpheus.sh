#!/bin/sh
# Install morpheus from pre-built binaries

# update package lists
apt-get update

# Update to latest version
VERSION=2.3.4

wget https://imc.zih.tu-dresden.de/morpheus/packages/dists/static/morpheus-${VERSION} --no-check-certificate
mkdir -p bin
mv morpheus-${VERSION} bin/morpheus
chmod u+x bin/morpheus
