import pandas as pd
from pyabc import History


def plot_distance_hist(
    history: History,
    population_index=None,
    col: str = "distance",
    title: str = "Histogram",
    data_label: str = None,
    color: str = "b",
    alpha: float = 0.5,
):
    """
    Histogram plot for the distance function for each particles

    Parameters
    ----------
    history: pyABC.History
         The history to extract data from.
    population_index: int
         Index of population to be plotted. If nothing is giving, then the
         last population will be selected.
    col: str (default = distance
         Column name for the distance value of particles.
    title: str (Histogram)
         Title for the plot.
    data_label: str
         Label for histogram. Valuable in cases of many histograms.
    color: str
         Color of the histogram.
    alpha: float
         Set the alpha value used for blending

    Returns
    -------
    ax: matplotlib axis
        axis of the plot
    """
    data_frame = pd.DataFrame
    if not population_index:
        data_frame = history.get_weighted_distances()
    else:
        data_frame = history.get_weighted_distances(population_index)
    df_list = data_frame[col]
    ax = df_list.plot.hist(bins=25, label=data_label, alpha=alpha, color=color)
    ax.set_title(title)
    ax.legend()
    return ax
