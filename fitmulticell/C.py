"""Constants."""

# TODO replace by petab constants
LIN = "lin"
LOG = "log"
LOG2 = "log2"
LOG10 = "log10"

MORPHEUS = "morpheus"
GUI_MORPHEUS = "morpheus-gui"
TIME = "time"
OUTPUT_FILE = "logger.csv"
