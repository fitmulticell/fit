import numpy as np


def eucl_dist(sim, obs):
    total = 0
    for key in sim:
        if key in ('loc', "time", "space.x"):
            continue
        x = np.array(sim[key])
        y = np.array(obs[key])
        if x.size != y.size:
            return np.inf

        total += np.sum((x - y) ** 2)
    return total

