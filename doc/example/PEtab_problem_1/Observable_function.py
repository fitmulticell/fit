def sumstat_a(sumstat):
    new_sumstat = {}
    for key, val in sumstat.items():
        if key in ["loc", "i", "time", "space.x"]:
            continue
        new_sumstat[key] = sumstat[key]
    return new_sumstat


def sumstat_i(sumstat):
    new_sumstat = {}
    for key, val in sumstat.items():
        if key in ["loc", "a", "time", "space.x"]:
            continue
        new_sumstat[key] = sumstat[key]
    return new_sumstat


# if __name__ == "__main__":
#    sumstat = sys.argv[1]
#    main(sumstat)
