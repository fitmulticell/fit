.. fitmulticell documentation master file, created by
   sphinx-quickstart on Wed Oct  9 15:52:16 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to fitmulticell's documentation!
========================================


.. image:: https://badge.fury.io/py/fitmulticell.svg
   :target: https://badge.fury.io/py/fitmulticell
   :alt: PyPI
.. image:: https://readthedocs.org/projects/fitmulticell/badge/?version=latest
   :target: https://fitmulticell.readthedocs.io/en/latest
   :alt: Documentation
.. image:: https://gitlab.com/fitmulticell/fit/badges/main/pipeline.svg
   :target: https://gitlab.com/fitmulticell/fit/-/commits/main
   :alt: CI pipeline
.. image:: https://codecov.io/gl/fitmulticell/fit/branch/main/graph/badge.svg?token=EIW43PMPMF
   :target: https://codecov.io/gl/fitmulticell/fit
   :alt: Code coverage
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
   :alt: Code style: Black

Source code: https://gitlab.com/fitmulticell/fit


.. image:: logo/logo_long.svg.png
   :alt: FitMultiCell logo
   :align: center


An Integrated Platform for Data-Driven Modeling of Multi-Cellular Processes. It delivers that by combining the simulation tool `morpheus <https://morpheus.gitlab.io>`_ and the likelihood-free inference tool `pyABC <https://github.com/icb-dcm/pyabc>`_.

The overall aim of FitMultiCell is to build and validate an open platform for modelling, simulation and parameter estimation of multicellular systems, which will be utilised to mechanistically answer biomedical questions based on imaging data.



.. toctree::
   :maxdepth: 3
   :caption: User's guide

   install
   running_fitmulticell
   example
   container
   youtube


.. toctree::
   :maxdepth: 3
   :caption: About

   changelog
   contact
   license
   logo


.. toctree::
   :maxdepth: 3
   :caption: API reference

   api


.. toctree::
   :maxdepth: 3
   :caption: Developer's guide

   contribute
   deploy


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
