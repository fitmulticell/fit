.. _youtube:

Youtube tutorial series
=======================

In this tutorial series, we exlain different functionalities of the FitMultiCell pipeline.
Here is a link to the playlist containing all parts of the series:
https://youtube.com/playlist?list=PLRgo5axBHp5jWwqSHa_XpkAJ2YnxRphCi.

Part 1
------

The first part of this tutorial series is an explanation of how to install the pipeline with its different components:
https://youtu.be/0qVro-hkyr0.

Part 2
------

The second part explains the different elements that need to be prepared by the user before starting the fitting process:
https://youtu.be/Nq0yQI2gZRQ.

Part 3
------

The third part executes one of the application examples from the FitMultiCell documentation.
This example is for fitting two parameters in a cell motility model:
https://youtu.be/4tsNzU7dtdw.
To read more about the model, please check this jupyter notebook:
https://fitmulticell.readthedocs.io/en/latest/example/Demo_CellMotility.html.

Part 4
------

The fourth part explains how to run the FitMultiCell pipeline in a large cluster using the distributed memory system parallelization strategy:
https://youtu.be/w9s0aS3sDVE.

Part 5
------

The fifth part explains how to run the visualisation server is part of the FitMultiCell pipeline in order to plot different diagnostic plot that helps to assess the fitting quality:
https://youtu.be/0nwJkU1dyd4.
