API reference
=============

.. automodule:: fitmulticell

.. toctree::
   :maxdepth: 3

   api_model
