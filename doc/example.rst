.. _example:

Examples
========

Getting started
---------------

.. toctree::
   :maxdepth: 2

   example/minimal.ipynb
   example/SBML_example.ipynb

PEtab
-----

.. toctree::
   :maxdepth: 2

   example/PEtab_example.ipynb
   example/PEtab_multiple_conditions.ipynb

Application examples
--------------------

.. toctree::
   :maxdepth: 2

   example/Demo_CellMotility.ipynb
   example/Classification.ipynb

Summary statistics
------------------

.. toctree::
   :maxdepth: 2

   example/Cluster_count.ipynb
   example/Cluster_size.ipynb
   example/Count_active_infected_cell.ipynb
   example/Count_Cell_types.ipynb
