# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------

project = 'FitMultiCell'
copyright = '2019, The FitMultiCell developers'
author = 'The FitMultiCell developers'

# -- General configuration ---------------------------------------------------

# required for readthedocs to have package information
import os
import sys

sys.path.insert(0, os.path.abspath('../'))

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.mathjax',
    # link to code
    'sphinx.ext.viewcode',
    # intersphinx
    'sphinx.ext.intersphinx',
    # numpy style docstrings
    'sphinx.ext.napoleon',
    'nbsphinx',
    # code highlighting in jupyter cells
    'IPython.sphinxext.ipython_console_highlighting',
    'sphinx.ext.autosummary',
]

# default autodoc options
# list for special-members seems not to be possible before 1.8
autodoc_default_options = {
    'members': True,
    'special-members': '__init__, __call__',
    'show-inheritance': True,
}

# links for intersphinx
intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
    'numpy': ('https://numpy.org/devdocs/', None),
    'scipy': ('https://docs.scipy.org/doc/scipy/reference/', None),
    'pandas': ('https://pandas.pydata.org/pandas-docs/dev', None),
    'petab': ('https://petab.readthedocs.io/en/stable/', None),
    'amici': ('https://amici.readthedocs.io/en/latest/', None),
    'pyabc': ('https://pyabc.readthedocs.io/en/latest/', None),
}

# the suffix(es) of source filenames
source_suffix = ['.rst']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = [
    '_build',
    'Thumbs.db',
    '.DS_Store',
    '**.ipynb_checkpoints',
]

# the master toctree document
master_doc = 'index'

# version
import fitmulticell

version = fitmulticell.__version__
release = version

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
html_title = "FitMultiCell documentation"

# A shorter title for the navigation bar.  Default is the same as html_title.
html_short_title = "FitMultiCell"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']
