Contribute
==========

Workflow
--------

If you start working on a new feature or a fix, please create an
`issue <https://gitlab.com/fitmulticell/fit/-/issues>`_,
shortly describing the issue, and assign yourself.
Your startpoint should always be the ``develop`` branch, which contains the
lastest updates.

Create an own branch or fork, on which you can implement your changes. To
get your work merged, please:

1. create a pull request to the ``develop`` branch with a meaningful summary,
2. check that code changes are covered by tests, and all tests pass,
3. check that the documentation is up-to-date,
4. request a code review from the main developers.

Merge requests to develop should be squashed-and-merged, while merge requests
(from develop) to main should be merged. This is to keep the history clean.

Environment
-----------

If you contribute to the development of FitMultiCell, consider installing
developer requirements, for e.g. local testing, via::

    pip install -r requirements-dev.txt

This installs the tools described below.

Pre-commit hooks
~~~~~~~~~~~~~~~~

Firstly, this installs a `pre-commit <https://pre-commit.com/>`_ tool.
To add those hooks to the `.git` folder of your local clone such that they are
run on every commit, run::

    pre-commit install

When adding new hooks, consider manually running ``pre-commit run --all-files``
once as usually only the diff is checked. The configuration is specified in
``.pre-commit-config.yaml``.

Should it be necessary to perform commits without pre-commit verification,
use ``git commit --no-verify`` or the shortform ``-n``.

Tox
~~~

Secondly, this installs the virtual testing tool
`tox <https://tox.readthedocs.io/en/latest/>`_, which we use for all tests,
format and quality checks. Its configuration is specified in ``tox.ini``.
To run it locally, simply execute::

    tox [-e flake8,doc]

with optional ``-e`` options specifying the environments to run, see
``tox.ini`` for defaults. tox creates virtual images in a ``.tox/`` subfolder.

GitLab CI/CD
------------

For automatic continuous integration testing, we use GitLab CI/CD
All tests are run there on pull requests and required to pass.
The configuration is specified in ``.gitlab-ci.yml``.

Documentation
-------------

To make FitMultiCell easily usable, we try to provide good documentation,
including code annotation and usage examples.
The documentation is hosted at
https://fitmulticell.readthedocs.io/en/latest/,
a version for the ``develop`` branch at
https://fitmulticell.readthedocs.io/en/develop/.
These are updated automatically on merges to the main and develop branches.
To create the documentation locally, run::

    tox -e doc

The documentation is then undre ``doc/_build``.
Alternatively, install the requirements via::

    pip install .[doc]

and then compile the documentation via::

    cd doc
    make html

When adding code, all modules, classes, functions, parameters, code blocks
should be properly documented.
When adding examples, these should be properly embedded.

Unit tests
----------

Unit tests are located in the ``test`` folder. All files starting with
``test_`` contain tests and are automatically run on GitLab CI/CD.
Run them locally via e.g.::

    tox -e base

You can also run only specific tests, see e.g. the
`pytest documentation <https://docs.pytest.org/en/latest/>`_
for details.
Unit tests can be written with pytest
or `unittest <https://docs.python.org/3/library/unittest.html>`_.

Code changes should always be covered by unit tests.
It might not always be easy to test code which is based on random sampling,
but we still encourage general sanity and integration tests.
We highly encourage a
`test-driven development <http://en.wikipedia.org/wiki/Test-driven_development>`_
style.

PEP8
----

We try to respect the `PEP8 <https://www.python.org/dev/peps/pep-0008>`_
coding standards. We run `flake8 <https://flake8.pycqa.org>`_ as part of the
tests. The flake8 plugins used are specified in ``tox.ini`` and the flake8
configuration is given in ``.flake8``. You can run the checks locally via::

    tox -e flake8
