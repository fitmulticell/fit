FitMultiCell
============

.. figure:: https://gitlab.com/fitmulticell/fit/raw/main/doc/logo/logo_long.svg.png
   :alt: FitMultiCell logo
   :width: 60 %
   :align: center

|pypi| |docs| |ci| |codecov| |black|

Fitting of multi-cellular models combining the tools `morpheus <https://gitlab.com/morpheus.lab/morpheus>`_ and `pyABC <https://github.com/icb-dcm/pyabc>`_.

- **Documentation:** https://fitmulticell.readthedocs.io
- **Contact:** https://fitmulticell.readthedocs.io/en/latest/contact.html
- **Source code:** https://gitlab.com/fitmulticell/fit
- **Examples:** https://fitmulticell.readthedocs.io/en/latest/example.html
- **Bug reports:** https://gitlab.com/fitmulticell/fit/issues

.. |pypi| image:: https://badge.fury.io/py/fitmulticell.svg
   :target: https://badge.fury.io/py/fitmulticell
   :alt: PyPI

.. |docs| image:: https://readthedocs.org/projects/fitmulticell/badge/?version=latest
   :target: http://fitmulticell.readthedocs.io/en/latest/
   :alt: Docs

.. |ci| image:: https://gitlab.com/fitmulticell/fit/badges/main/pipeline.svg
   :target: https://gitlab.com/fitmulticell/fit/-/commits/main
   :alt: CI pipeline

.. |codecov| image:: https://codecov.io/gl/fitmulticell/fit/branch/main/graph/badge.svg?token=EIW43PMPMF
   :target: https://codecov.io/gl/fitmulticell/fit

.. |black| image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
   :alt: Code style: Black
